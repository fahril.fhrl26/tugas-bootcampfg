// Div class content
const divContent = document.querySelector("div.content");

divContent.style.backgroundColor = "lightblue";
divContent.style.padding = "20px 30px";

// Judul h1
const h1 = document.querySelector("h1");
h1.innerText = "JUDUL 1";
h1.style.textAlign = "center";

// paragraphs
const paragraphs = document.querySelectorAll("p");
for (let i = 0; i < paragraphs.length; i++) {
  paragraphs[i].innerText = "Bukan Lorem Lagi";
  paragraphs[i].style.color = "#666";
}

// link
const links = document.querySelectorAll("ul li a");

for (let link in links) {
  let index = link;
  if (link == 0) {
    links[link].innerText = `List 1`;
    links[link].style.color = `red`;
  } else if (link == 1) {
    links[link].innerText = `List 2`;
    links[link].style.color = `blue`;
  } else if (link == 2) {
    links[link].innerText = `List 3`;
    links[link].style.color = `yellow`;
  } else if (link == 3) {
    links[link].innerText = `List 4`;
    links[link].style.color = `green`;
  } else if (link == 4) {
    links[link].innerText = `List 5`;
    links[link].style.color = `grey`;
  }
}
