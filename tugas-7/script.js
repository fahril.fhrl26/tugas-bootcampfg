// menambahkan font-family
const body = document.body;
body.style.fontFamily = `Arial`;

// membuat paragraph dan memasukan ke dalam body
const paragraph = document.createElement("p");
paragraph.innerHTML =
  "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Atque maxime quos natus! Odio, vel. In necessitatibus voluptates eaque officia praesentium quis quo architecto libero tempore sequi totam numquam quibusdam molestiae enim voluptatum provident doloremque cumque id nobis, ullam voluptas ea perferendis consectetur! Sed iusto, quasi esse mollitia hic similique iste.";
body.append(paragraph);

// membuat img dan memasukan ke dalam body
const image = document.createElement("img");
image.setAttribute("src", "image/image.jpg");
image.setAttribute("width", "500px");
body.append(image);

// membuat h2 dan memasukan ke dalam body
const h2 = document.createElement("h2");
h2.innerHTML = `Our Partners`;
body.append(h2);

// membuat list dan memasukan ke dalam ul
const ul = document.createElement("ul");
const li1 = document.createElement("li");
const li2 = document.createElement("li");
const li3 = document.createElement("li");

ul.append(li1);
ul.append(li2);
ul.append(li3);

// membuat link
const link1 = document.createElement("a");
const link2 = document.createElement("a");
const link3 = document.createElement("a");

link1.innerHTML = "Facebook";
link2.innerHTML = "Instagram";
link3.innerHTML = "Youtube";

link1.setAttribute("href", "https://www.facebook.com");
link2.setAttribute("href", "https://www.instagram.com");
link3.setAttribute("href", "https://www.youtube.com");

link1.setAttribute("target", "_blank");
link2.setAttribute("target", "_blank");
link3.setAttribute("target", "_blank");

// memberi style link
link1.style.color = "red";
link2.style.color = "blue";
link3.style.color = "green";

// memasukan link ke dalam element li
li1.append(link1);
li2.append(link2);
li3.append(link3);

// memasukan ul ke dalam body
body.append(ul);

// membuat h3 dan menambahkan ke dalam body
const h3 = document.createElement("h3");
h3.innerHTML = `Contact Us`;
h3.style.fontWeight = "400";

body.append(h3);

// membuat form
const form = document.createElement("form");

// membuat label dan input
const labelNama = document.createElement("label");
const inputNama = document.createElement("input");
labelNama.innerHTML = "Nama ";
labelNama.setAttribute("for", "nama");
inputNama.setAttribute("id", "nama");

const labelEmail = document.createElement("label");
const inputEmail = document.createElement("input");
labelEmail.innerHTML = "Email ";
labelEmail.setAttribute("for", "email");
inputEmail.setAttribute("id", "email");

const labelPhone = document.createElement("label");
const inputPhone = document.createElement("input");
labelPhone.innerHTML = "Phone ";
labelPhone.setAttribute("for", "phone");
inputPhone.setAttribute("id", "phone");

// memasukan label dan input kedalam element form
form.append(labelNama);
form.append(inputNama);
form.append(labelEmail);
form.append(inputEmail);
form.append(labelPhone);
form.append(inputPhone);

// menambahkan br
const br1 = document.createElement("br");
const br2 = document.createElement("br");
inputNama.after(br1);
inputEmail.after(br2);

// memasukan form ke dalam body
body.append(form);

// membuat button
const buttonReset = document.createElement("button");
const buttonSubmit = document.createElement("button");
buttonReset.setAttribute("type", "reset");
buttonSubmit.setAttribute("type", "submit");
buttonReset.innerHTML = "Reset";
buttonSubmit.innerHTML = "Submit";

// button style
buttonReset.style.backgroundColor = "white";
buttonSubmit.style.backgroundColor = "blue";
buttonReset.style.border = "1px solid black";
buttonSubmit.style.border = "1px solid black";
buttonSubmit.style.color = "white";
buttonReset.style.padding = "5px 10px";
buttonSubmit.style.padding = "5px 10px";
buttonReset.style.borderRadius = "3px";
buttonSubmit.style.borderRadius = "3px";
buttonReset.style.margin = "10px 5px 0 0";
buttonSubmit.style.margin = "10px 0 0 5px";
buttonReset.style.cursor = "pointer";
buttonSubmit.style.cursor = "pointer";

// memasukan button ke dalam body
body.append(buttonReset);
body.append(buttonSubmit);
