// Show modal
const addData = document.querySelector(".container button");
const modal = document.querySelector(".modal-container");

const showModal = () => {
  modal.classList.add("active");
};

addData.onclick = showModal;

// close modal
const closeButton = document.querySelector(".buttons button:first-child");

const closeModal = () => {
  modal.classList.remove("active");
};

closeButton.onclick = closeModal;

const submit = document.querySelector(".buttons button:last-child");
submit.addEventListener("click", (event) => {
  event.preventDefault();
});
